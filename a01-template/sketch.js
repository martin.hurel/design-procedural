let rectColor;
let bgColor;
let rectWidth;
let rectHeight;
let startX; 
let startY; 
let seed;
let randomRectNumber;
let rectWidthInit; 
let rectHeightInit; 
function setup(){
    createCanvas(600, 600);
     rectColor = color(237, 0, 62);  
     bgColor = color(163, 167, 174);
     randomRectWidthNumber = floor(random(100));
     randomRectHeightNumber = floor(random(100));
     rectWidthInit = 600;
     rectHeightInit = 600;
     rectWidth = (rectWidthInit/randomRectWidthNumber);
     rectHeight = (rectHeightInit/randomRectHeightNumber);
     startX = 0; 
     startY = 0; 
     seed = random(10000)
    background(bgColor);
    // random(5); // Nombre entre 0 et 4
    // random(1, 12); // Nombre entre 1 et 11
    // random(); // Nombre entre 0 et 1

}    


function draw() {
    //Fond
    background(bgColor);
    noStroke();
    fill(rectColor); //couleur de remplissage 
    randomSeed(seed)
    
    for(let i=0; i<randomRectWidthNumber; i++){
        fill(random(255), random(255), random(255)); 
        rect(startX + rectWidth * i + 1, startY, rectWidth, random(rectHeightInit /2));
    }

    for(let i=0; i<randomRectWidthNumber; i++){
        fill(random(255), random(255), random(255)); 
        rect(startX + rectWidth * i + 1, 600, rectWidth, random(- rectHeightInit/2));
    }

    for(let i=0; i<randomRectHeightNumber; i++){
        fill(random(255), random(255), random(255)); 
        rect(startX, startY + rectHeight * i + 1, random(rectWidthInit /2), rectHeight);
        
    }

    for(let i=0; i<randomRectHeightNumber; i++){
        fill(random(255), random(255), random(255)); 
        rect(600, startY + rectHeight * i + 1, random(-rectWidthInit/2), rectHeight);
        
    }

 setTimeout(function(){  document.location.reload(true) }, 500);
}

function keyTyped(){
    if (key === 'n'){
        seed = random(10000);
    }
    else if (key === 'r'){
        document.location.reload(true);
    }
    else if (key === 's'){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = mm + '-' + dd + '-' + yyyy;
        saveCanvas('mySketch-'+ today + '.jpg');
    }
}